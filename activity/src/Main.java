import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

//        First name
        System.out.println("First Name:");
        String firstName = myObj.nextLine();
        System.out.println(firstName);

//        Last name
        System.out.println("Last Name:");
        String lastName = myObj.nextLine();
        System.out.println(lastName);

//        First subject grade
        System.out.println("First Subject Grade:");
        double firstSubjGrade = myObj.nextDouble();
        System.out.println(firstSubjGrade);

//        Second subject grade
        System.out.println("Second Subject Grade:");
        double secondSubjGrade = myObj.nextDouble();
        System.out.println(secondSubjGrade);

//        Third subject grade
        System.out.println("Third Subject Grade:");
        double thirdSubjGrade = myObj.nextDouble();
        System.out.println(thirdSubjGrade);

        System.out.println("Good day, " + firstName + " " + lastName + ".");

        double aveGrade = ((firstSubjGrade + secondSubjGrade + thirdSubjGrade) / 3);
        int roundedAveGrade = (int) Math.round(aveGrade);
        System.out.println("Your grade average is: " + roundedAveGrade);
    }
}